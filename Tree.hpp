/*
Michael Ashmead, 600.120, 4/15/15, Homework 6, 484-682-9355, mashmea1, ashmeadmichael@gmail.com
*/

#ifndef _TREE_H
#define _TREE_H

#include <cstdlib>
#include <string>
#include <sstream>
#include <iostream>

// tree of characters, can be used to implement a trie

template <class T> class Tree {
	friend class TreeTest_Int;
	friend class TreeTest_Double;
	friend class TreeTest_String;
	friend class TreeTest_FileDir;

	T data; // the value stored in the tree node

	Tree<T> * kids;  // children - pointer to first child of list, maintain order & uniqueness

	Tree<T> * sibs;  // siblings - pointer to rest of children list, maintain order & uniqueness
                      // this should always be null if the object is the root of a tree

  	Tree<T> * prev;  // pointer to parent if this is a first child, or left sibling otherwise
                      // this should always be null if the object is the root of a tree

	public:
  	Tree(T ch);
  	Tree(const  Tree&);  //deep copy constructor

	~Tree();  // clear siblings to right and children and this node
  
  	// siblings and children must be unique, return true if added, false otherwise
  	bool addChild(T ch);
  	bool removeChild(T ch);

  	// add tree root for better building, root should have null prev and sibs 
  	// returns false on any type of failure, including invalid root
  	bool addChild(Tree<T>* root);

  	std::string toString(); // all characters, separated by newlines, including at the end

 	private:
  	// these should only be called from addChild, and have the same restrictions
  	// the root of a tree should never have any siblings
  	// returns false on any type of failure, including invalid root
  	bool addSibling(T ch);
  	bool addSibling(Tree *root);
  	Tree<T>* copyTreeHelper(const Tree<T>* root, Tree<T>* oldroot);
};

#endif

template <class T> Tree<T>::Tree(T ch):data(ch), kids(nullptr), sibs(nullptr), prev(nullptr) {} //constructor

//deep copy constructor
template <class T> Tree<T>::Tree(const Tree& fromTree):data(fromTree.data), kids(fromTree.kids), sibs(fromTree.sibs), prev(fromTree.prev) {
	this->kids = copyTreeHelper(this->kids, this);
}

template <class T> Tree<T>* Tree<T>::copyTreeHelper(const Tree<T>* root, Tree<T>* oldroot) {
	if (root == NULL) {
		return NULL;
	}
	else { //copy root node
		Tree<T>* newNode = new Tree(root->data);
		newNode->prev = oldroot;
		newNode->kids = copyTreeHelper(root->kids, newNode);
		newNode->sibs = copyTreeHelper(root->sibs, newNode);
		return newNode;
	}	
}

template <class T> Tree<T>::~Tree() { //destructor
	if (kids != NULL)
		delete kids;
	if (sibs != NULL)
		delete sibs;
}

//removes a child node (and its children) of a node
template <class T> bool Tree<T>::removeChild(T ch) {
	//look for the node we want to remove
	//return false if not found
	bool directChild = true;
	Tree* exNode = this->kids;
	
	while (exNode != NULL && exNode->data != ch) {
		directChild = false;
		exNode = exNode->sibs;
	}
	if (exNode == NULL || exNode->data != ch)
		return false;
	
	//if the child node that we are deleting is a "direct child"
	if (directChild) {
		//if has sibling set it prev->kids to sibling and set sib->prev to child's previous
		if (exNode->sibs != NULL) {
			exNode->prev->kids = exNode->sibs;
			exNode->sibs->prev = exNode->prev;
			exNode->sibs = NULL;
			exNode->~Tree(); //when we call this function it is deleting its siblings too, so set its sibs to null above
		}
		//if no sibling set it prev->kids to null and call destructor on that node
		else {				
			exNode->prev->kids = NULL;
			exNode->~Tree();
		}
	}
	//if the child node that we are deleting is not a "direct child"
	else {
		//if has sibling
		if (exNode->sibs != NULL) {
			//set it prev->sibs to child's sibs
			exNode->prev->sibs = exNode->sibs;
			//set it sibs->prev to child's prev
			exNode->sibs->prev = exNode->prev;
			//call destructor
			exNode->sibs = NULL;
			exNode->~Tree(); //when we call this function it is deleting its siblings too, so set its sibs to null above
		}
		//if no sibling
		else {
			//set it prev->sibs to NULL
			exNode->prev->sibs = NULL;
			//call destructor
			exNode->~Tree();
		}
	}
	return true;
}

template <class T> std::string Tree<T>::toString() {
	std::ostringstream oss; //keep adding characters onto this
	oss << data << "\n"; //add current character onto string
	if (kids != NULL)
		oss << kids->toString();
	if (sibs != NULL)
		oss << sibs->toString();
	return oss.str();
}

template <class T> bool Tree<T>::addChild(T ch) {
	//check if current node child pointer is empty
	if (this->kids == NULL) { //it is so add new T under it
		this->kids = new Tree<T>(ch);
		this->kids->prev = this;
		return true;
	}
	//check if new character is already child of current node
	else if (kids->data == ch) {
		return false;
	}
	//make ch the child of this node and "slide siblings to the right"
	else if (ch < this->kids->data) {
		Tree<T>* newNode = new Tree<T>(ch);
		newNode->sibs = this->kids;
		newNode->prev = this;
		this->kids = newNode;
		return true;
	}
	else if (kids->prev != NULL)  { //make this character a sibling of the child node since it is not a root node
		return this->kids->addSibling(ch);
	}
	else {
		return false;
	}	
}

template <class T> bool Tree<T>::addChild(Tree *root) { //root is the thing that will be made the root of this current tree
	//make sure root is a root
	if (root->sibs != NULL || root->prev != NULL)
		return false;
	//checks if current node child pointer is empty
	else if (this->kids == NULL) { //it is so add root under it
		this->kids = root;
		root->prev = this;
		return true;
	}
	else if (kids->data == root->data) {
		return false;
	}
	else if (root->data < this->kids->data) {
		root->sibs = this->kids;
		root->prev = this;
		this->kids = root;
		return true;
	}
	else if (prev != NULL) {
		return this->addSibling(root);
	}
	else {
		return false;
	}
}

template <class T> bool Tree<T>::addSibling(T ch) {
	Tree<T>* newNode = new Tree<T>(ch);
	return this->addSibling(newNode);
}

template <class T> bool Tree<T>::addSibling(Tree *root) {
	//make sure root is a root
	if (root->sibs != NULL || root->prev != NULL) {
		delete root;
		return false;
	}
	//make sure we are not adding a sibling to a root
	else if (this->sibs == NULL && this->prev == NULL) {
		delete root;
		return false;
	}
	else if (root->data == this->data) {
		delete root;
		return false;
	}
	else if (root->data < this->data) { //insert node
		root->sibs = this; //give inserted node old node's siblings
		root->prev = this->prev;
		this->prev = root;
		root->prev->sibs = root;
		return true;
	}
	else if (this->sibs == NULL) { //make root a sibling
		this->sibs = root;
		root->prev = this;
		return true;
	}
	else //keep moving through siblings
		return this->sibs->addSibling(root);
}
