/*
Michael Ashmead, 600.120, 4/15/15, Homework 6, 484-682-9355, mashmea1, ashmeadmichael@gmail.com
*/

#include "FileDir.h"

FileDir::FileDir(string n, long s, bool f):name(n), size(s), file(f) {}

//copy constructor
FileDir::FileDir(const FileDir& fromFileDir):name(fromFileDir.name), size(fromFileDir.size), file(fromFileDir.file) {}

string FileDir::rename(string n) {
	string temp = name;
	name = n;
	return temp;
}

long FileDir::resize(long s) {
	if (size + s >= 0) //make sure subtraction will not go below 0
		size += s;
	return size;	
}

bool FileDir::operator<(const FileDir& f) {
	if (this->isFile() == false && f.isFile() == true) //directories come before files
		return true;
	if (this->isFile() == true && f.isFile() == false)
		return false;
	string thistemp = this->getName();
	transform(thistemp.begin(), thistemp.end(), thistemp.begin(), ::tolower);
	string ftemp = f.getName();
	transform(ftemp.begin(), ftemp.end(), ftemp.begin(), ::tolower);
	if (thistemp < ftemp)
		return true;
	return false;
		
}

bool FileDir::operator==(const FileDir& f) {
	if (this->getName() == f.getName() && this->isFile() == f.isFile())
		return true;
	return false;
}

ostream& operator<<(ostream& output, const FileDir& f) {
	output << f.toString();
	return output;
}
