//Michael Ashmead, 600.120, 4/15/15, Homework 6, 484-682-9355, mashmea1, ashmeadmichael@gmail.com

#include "Tree.hpp"
#include "FileDir.h"
#include <string>
#include <cassert>
#include <iostream>

using std::cout;
using std::endl;

class TreeTest_Int {
	public:
	static void constructorTest() {
        	//build a few trees with constructor
        	Tree<int> t1(5);
        	assert(t1.toString() == "5\n");
        	Tree<int> t2(-1);
        	assert(t2.toString() == "-1\n");
        	Tree<int> t3(0);
        	assert(t3.toString() == "0\n");
    	}

	static void addsTest() {
		// 2
                Tree<int>* t1Ptr = new Tree<int>(2);
        	Tree<int>& t1 = *t1Ptr; //avoids a bunch of *s below
        	assert(t1.toString() == "2\n");

        	// 2
        	// |
        	// 5
        	assert(t1.addChild(5));
        	assert(t1.toString() == "2\n5\n");
        	//can't add again
        	assert(!t1.addChild(5));
        	assert(t1.toString() == "2\n5\n");

        	// 2
        	// |
        	// 5 - 9
        	assert(t1.addChild(9));
        	assert(t1.toString() == "2\n5\n9\n");
        	//can't add again
        	assert(!t1.addChild(9));
        	assert(t1.toString() == "2\n5\n9\n");

        	// 2
        	// |
        	// 5 - 9 - 12
        	assert(t1.addChild(12));
               	assert(t1.toString() == "2\n5\n9\n12\n");
        	//can't add repeats
        	assert(!t1.addChild(5));
        	assert(!t1.addChild(9));
        	assert(!t1.addChild(12));
        	assert(t1.toString() == "2\n5\n9\n12\n");

		delete t1Ptr;
    	}
	
	//adds a single child
	static void addSimpleChildTest() {
        	// 20
        	Tree<int>* t1 = new Tree<int>(20);
        	assert(t1->toString() == "20\n");

        	// 20
        	Tree<int>* t2 = new Tree<int>(20);
        	assert(t2->toString() == "20\n");

        	// 20
        	// |
        	// 20
        	assert(t2->addChild(t1));
        	assert(t2->toString() == "20\n20\n");

        	delete t2;
    	}

    	static void deepCopyTest() {
		//test making deep copy of a CTree with only a root node
		Tree<int>* t0 = new Tree<int>(20);
		assert(t0->toString() == "20\n");

		Tree<int> t0copy(*t0);
		assert(t0copy.toString() == t0->toString());

		delete t0;
		assert(t0copy.toString() == "20\n");

		//test making deep copy of a large CTree
		Tree<int>* t1Ptr = new Tree<int>(20);
        	Tree<int>& t1 = *t1Ptr; // avoids a bunch of *'s below
        	assert(t1.addChild(22));
        	assert(t1.addChild(23));
        	assert(t1.addChild(21));
        	Tree<int>* t2Ptr = new Tree<int>(1);
        	Tree<int>& t2 = *t2Ptr;
        	assert(t2.addChild(29));
        	assert(t2.addChild(51));
        	assert(t2.addChild(21));
        	assert(t2.addChild(&t1));
        	assert(t2.addChild(4));
        	assert(t2.addChild(50));
        	// 1
        	// |
        	// 4 - 20 - 21 - 29 - 50 - 51 - 100
        	//     |
        	//     21 - 22 - 23
        	assert(t2.addChild(100));
        	assert(t2.toString() == "1\n4\n20\n21\n22\n23\n21\n29\n50\n51\n100\n");
		Tree<int> t2copy(t2);
		assert(t2copy.toString() == t2.toString());
		delete &t2;
		assert(t2copy.toString() == "1\n4\n20\n21\n22\n23\n21\n29\n50\n51\n100\n");
    	}
};

class TreeTest_Double {
	public:
	static void constructorTest() {
        	//build a few trees with constructor
        	Tree<double> t1(5);
        	assert(t1.toString() == "5\n");
        	Tree<double> t2(-1);
        	assert(t2.toString() == "-1\n");
        	Tree<double> t3(0);
        	assert(t3.toString() == "0\n");
    	}

	static void addsTest() {
		// 2
                Tree<double>* t1Ptr = new Tree<double>(2);
        	Tree<double>& t1 = *t1Ptr; //avoids a bunch of *s below
        	assert(t1.toString() == "2\n");

        	// 2
        	// |
        	// 5
        	assert(t1.addChild(5));
        	assert(t1.toString() == "2\n5\n");
        	//can't add again
        	assert(!t1.addChild(5));
        	assert(t1.toString() == "2\n5\n");

        	// 2
        	// |
        	// 5 - 9
        	assert(t1.addChild(9));
        	assert(t1.toString() == "2\n5\n9\n");
        	//can't add again
        	assert(!t1.addChild(9));
        	assert(t1.toString() == "2\n5\n9\n");

        	// 2
        	// |
        	// 5 - 9 - 12
        	assert(t1.addChild(12));
               	assert(t1.toString() == "2\n5\n9\n12\n");
        	//can't add repeats
        	assert(!t1.addChild(5));
        	assert(!t1.addChild(9));
        	assert(!t1.addChild(12));
        	assert(t1.toString() == "2\n5\n9\n12\n");

		delete t1Ptr;
    	}
	
	//adds a single child
	static void addSimpleChildTest() {
        	// 20
        	Tree<double>* t1 = new Tree<double>(20);
        	assert(t1->toString() == "20\n");

        	// 20
        	Tree<double>* t2 = new Tree<double>(20);
        	assert(t2->toString() == "20\n");

        	// 20
        	// |
        	// 20
        	assert(t2->addChild(t1));
        	assert(t2->toString() == "20\n20\n");

        	delete t2;
    	}

    	static void deepCopyTest() {
		//test making deep copy of a CTree with only a root node
		Tree<double>* t0 = new Tree<double>(20);
		assert(t0->toString() == "20\n");

		Tree<double> t0copy(*t0);
		assert(t0copy.toString() == t0->toString());

		delete t0;
		assert(t0copy.toString() == "20\n");

		//test making deep copy of a large CTree
		Tree<double>* t1Ptr = new Tree<double>(20);
        	Tree<double>& t1 = *t1Ptr; // avoids a bunch of *'s below
        	assert(t1.addChild(22));
        	assert(t1.addChild(23));
        	assert(t1.addChild(21));
        	Tree<double>* t2Ptr = new Tree<double>(1);
        	Tree<double>& t2 = *t2Ptr;
        	assert(t2.addChild(29));
        	assert(t2.addChild(51));
        	assert(t2.addChild(21));
        	assert(t2.addChild(&t1));
        	assert(t2.addChild(4));
        	assert(t2.addChild(50));
        	// 1
        	// |
        	// 4 - 20 - 21 - 29 - 50 - 51 - 100
        	//     |
        	//     21 - 22 - 23
        	assert(t2.addChild(100));
        	assert(t2.toString() == "1\n4\n20\n21\n22\n23\n21\n29\n50\n51\n100\n");
		Tree<double> t2copy(t2);
		assert(t2copy.toString() == t2.toString());
		delete &t2;
		assert(t2copy.toString() == "1\n4\n20\n21\n22\n23\n21\n29\n50\n51\n100\n");
    	}
};

class TreeTest_String {
	public:
    	static void constructorTest() {
        	//build a few trees with constructor
        	Tree<std::string> t1("A");
        	assert(t1.toString() == "A\n");
        	Tree<std::string> t2("b");
        	assert(t2.toString() == "b\n");
        	Tree<std::string> t3("^");
        	assert(t3.toString() == "^\n");
    	}

    	static void addsTest() {
        	// A
        	Tree<std::string>* t1Ptr = new Tree<std::string>("A");
        	Tree<std::string>& t1 = *t1Ptr; //avoids a bunch of *s below
        	assert(t1.toString() == "A\n");

        	// A
        	// |
        	// b
        	assert(t1.addChild("b"));
        	assert(t1.toString() == "A\nb\n");
        	//can't add again
        	assert(!t1.addChild("b"));
        	assert(t1.toString() == "A\nb\n");

        	// A
        	// |
        	// b - c
        	assert(t1.addChild("c"));
        	assert(t1.toString() == "A\nb\nc\n");
        	//can't add again
        	assert(!t1.addChild("c"));
        	assert(t1.toString() == "A\nb\nc\n");

        	// A
        	// |
        	// B - b - c
        	assert(t1.addChild("B"));
        	//"B" comes before "b"
        	assert(t1.toString() == "A\nB\nb\nc\n");
        	//can't add repeats
        	assert(!t1.addChild("B"));
        	assert(!t1.addChild("b"));
        	assert(!t1.addChild("c"));
        	assert(t1.toString() == "A\nB\nb\nc\n");

        	//can't add 'A' as sibling of "A"
        	assert(!t1.addSibling("A"));
        	assert(t1.toString() == "A\nB\nb\nc\n");

        	//make sure that we can't add siblings to the root
        	assert(!t1.addSibling("C"));
        	assert(t1.toString() == "A\nB\nb\nc\n");

        	//adding in an already built subTree
        	//first build another tree
        	// R
        	Tree<std::string>* t2Ptr = new Tree<std::string>("R");
        	Tree<std::string>& t2 = *t2Ptr;
        	assert(t2.toString() == "R\n");
        
        	// R
        	// |
        	// C
        	assert(t2.addChild("C"));
        	assert(t2.toString() == "R\nC\n");

        	// R
        	// |
        	// C - d
        	assert(t2.addChild("d"));
        	assert(t2.toString() == "R\nC\nd\n");
        	//can't repeat
        	assert(!t2.addChild("d"));
        	assert(t2.toString() == "R\nC\nd\n");

        	// R
        	// |
        	// B - C - d
        	assert(t2.addChild("B"));
        	assert(t2.toString() == "R\nB\nC\nd\n");
        	//can't repeat
        	assert(!t2.addChild("B"));
        	assert(t2.toString() == "R\nB\nC\nd\n");

        	//add t1 in as a child
        	// R
        	// |
        	// A - B - C - d
        	// |
        	// B - b - c

        	//t1 is as before
        	assert(t1.toString() == "A\nB\nb\nc\n");
        	//add t1 to t2
        	assert(t2.addChild(&t1));
        	//t1 should now have siblings
        	assert(t1.toString() == "A\nB\nb\nc\nB\nC\nd\n");
        	//t2 should be updated
        	assert(t2.toString() == "R\nA\nB\nb\nc\nB\nC\nd\n");

        	// R
        	// |
        	// @ - A - B - C - d
        	//     |
        	//     B - b - c
        	assert(t2.addChild("@"));
        	assert(t2.toString() == "R\n@\nA\nB\nb\nc\nB\nC\nd\n");
        	//shouldn't be able to add duplicate children
        	assert(!t2.addChild("@"));
        	assert(!t2.addChild("A"));
        	assert(!t2.addChild("B"));
        	assert(!t2.addChild("C"));
        	assert(!t2.addChild("d"));

        	// R
        	// |
        	// @ - A - B - C - D - d
        	//     |
        	//     B - b - c
        	assert(t2.addChild("D"));
        	assert(t2.toString() == "R\n@\nA\nB\nb\nc\nB\nC\nD\nd\n");

        	// R
        	// |
        	// @ - A - B - C - D - d - e
        	//     |
        	//     B - b - c
        	assert(t2.addChild("e"));
        	assert(t2.toString() == "R\n@\nA\nB\nb\nc\nB\nC\nD\nd\ne\n");

        	delete t2Ptr;
	}

    	//adds a single child
    	static void addSimpleChildTest() {
        	// A
        	Tree<std::string>* t1 = new Tree<std::string>("A");
        	assert(t1->toString() == "A\n");

        	// A
        	Tree<std::string>* t2 = new Tree<std::string>("A");
        	assert(t2->toString() == "A\n");

        	// A
        	// |
        	// A
        	assert(t2->addChild(t1));
        	assert(t2->toString() == "A\nA\n");

        	delete t2;
    	}

    	static void deepCopyTest() {
		//test making deep copy of a Tree<string> with only a root node
		Tree<std::string>* t0 = new Tree<std::string>("A");
		assert(t0->toString() == "A\n");

		Tree<std::string> t0copy(*t0);
		assert(t0copy.toString() == t0->toString());

		delete t0;
		assert(t0copy.toString() == "A\n");

		//test making deep copy of a large Tree<string>
		Tree<std::string>* t1Ptr = new Tree<std::string>("A");
        	Tree<std::string>& t1 = *t1Ptr; // avoids a bunch of *'s below
        	assert(t1.addChild("b"));
        	assert(t1.addChild("c"));
        	assert(t1.addChild("B"));
        	Tree<std::string>* t2Ptr = new Tree<std::string>("R");
        	Tree<std::string>& t2 = *t2Ptr;        
        	assert(t2.addChild("C"));
        	assert(t2.addChild("d"));
        	assert(t2.addChild("B"));
        	assert(t2.addChild(&t1));
        	assert(t2.addChild("@"));
        	assert(t2.addChild("D"));
        	// R
        	// |
        	// @ - A - B - C - D - d - e
        	//     |
        	//     B - b - c
        	assert(t2.addChild("e"));
        	assert(t2.toString() == "R\n@\nA\nB\nb\nc\nB\nC\nD\nd\ne\n");
		Tree<std::string> t2copy(t2);
		assert(t2copy.toString() == t2.toString());
		delete &t2;
		assert(t2copy.toString() == "R\n@\nA\nB\nb\nc\nB\nC\nD\nd\ne\n");
    	}
};

class TreeTest_FileDir {
	public:
	static void constructorTest() {
        	//build a few trees with constructor
        	Tree<FileDir> t1(FileDir("testDir", 4, false));
        	assert(t1.toString() == "testDir [4kb]\n");
        	Tree<FileDir> t2(FileDir("Blah"));
        	assert(t2.toString() == "Blah [4kb]\n");
        	Tree<FileDir> t3(FileDir("y", 20, true));
        	assert(t3.toString() == "y/ [20kb]\n");
    	}

	static void addsTest() {
        	// A
        	Tree<FileDir>* t1Ptr = new Tree<FileDir>(FileDir("A"));
        	Tree<FileDir>& t1 = *t1Ptr; //avoids a bunch of *s below
        	assert(t1.toString() == "A [4kb]\n");

        	// A
        	// |
        	// b
        	assert(t1.addChild(FileDir("b")));
        	assert(t1.toString() == "A [4kb]\nb [4kb]\n");
        	//can't add again
        	assert(!t1.addChild(FileDir("b")));
        	assert(t1.toString() == "A [4kb]\nb [4kb]\n");

        	// A
        	// |
        	// b - c
        	assert(t1.addChild(FileDir("c")));
        	assert(t1.toString() == "A [4kb]\nb [4kb]\nc [4kb]\n");
        	//can't add again
        	assert(!t1.addChild(FileDir("c")));
        	assert(t1.toString() == "A [4kb]\nb [4kb]\nc [4kb]\n");

        	// A
        	// |
        	// B - b - c
        	assert(t1.addChild(FileDir("B", 3, true)));
        	//"B" comes before "b" since it is a directory
        	assert(t1.toString() == "A [4kb]\nB/ [3kb]\nb [4kb]\nc [4kb]\n");
        	//can't add repeats
        	assert(!t1.addChild(FileDir("B", 3, true)));
        	assert(!t1.addChild(FileDir("b")));
        	assert(!t1.addChild(FileDir("c")));
        	assert(t1.toString() == "A [4kb]\nB/ [3kb]\nb [4kb]\nc [4kb]\n");

		delete t1Ptr;
	}

    	//adds a single child
    	static void addSimpleChildTest() {
        	// A
        	Tree<FileDir>* t1 = new Tree<FileDir>(FileDir("A"));
        	assert(t1->toString() == "A [4kb]\n");

        	// A
        	Tree<FileDir>* t2 = new Tree<FileDir>(FileDir("A"));
        	assert(t2->toString() == "A [4kb]\n");

        	// A
        	// |
        	// A
        	assert(t2->addChild(t1));
        	assert(t2->toString() == "A [4kb]\nA [4kb]\n");

        	delete t2;
    	}
};

int main(void) {
	cout << "Testing Tree" << endl;

	TreeTest_Int::constructorTest();
	TreeTest_Int::addsTest();
	TreeTest_Int::addSimpleChildTest();
	TreeTest_Int::deepCopyTest();
	
	TreeTest_Double::constructorTest();
	TreeTest_Double::addsTest();
	TreeTest_Double::addSimpleChildTest();
	TreeTest_Double::deepCopyTest();
	
	TreeTest_String::constructorTest();
	TreeTest_String::addsTest();
	TreeTest_String::addSimpleChildTest();
	TreeTest_String::deepCopyTest();
	
	TreeTest_FileDir::constructorTest();
	TreeTest_FileDir::addsTest();
	TreeTest_FileDir::addSimpleChildTest();

	cout << "Tree tests passed" << endl;
}
