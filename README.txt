Michael Ashmead, 600.120, 4/15/15, Homework 6, 484-682-9355, mashmea1, ashmeadmichael@gmail.com

Hw6 Part A:
The program pasts all tests. It defines a class called FileDir that has 3 members: a name, a size, and a boolean type named file. There are getters for all 3 fields and there are also functions for printing out the function to a string, for renaming the instance, and for resizing the instance.

Hw6 Part B:
Use "make" to run the program.
Both extra credit options are implemented. Also the tests for the 2 new extra credit functions are implemented in the test file.
	- The removeChild function has memory leaks.

Hw7 Part C:
Use "make" to run the program.
The templated class is written in Tree.hpp. The tests of the templated class are still done in TreeTest.cpp.
Two primitives tested: int and double, along with string and FileDir objects.
	-Note: removed the removeChild function from Part B since it has memory leaks.
	-Note: also remove the deepCopy of FileDir Tree since I would have to modify the Tree.hpp function to deep copy FileDir objects too