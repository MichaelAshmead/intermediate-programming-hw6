/*
Michael Ashmead, 600.120, 4/15/15, Homework 6, 484-682-9355, mashmea1, ashmeadmichael@gmail.com
*/

#include "CTree.h"
#include <cassert>

CTree::CTree(char ch):data(ch), kids(nullptr), sibs(nullptr), prev(nullptr) {} //constructor

//deep copy constructor
CTree::CTree(const CTree& fromCTree):data(fromCTree.data), kids(fromCTree.kids), sibs(fromCTree.sibs), prev(fromCTree.prev) {
	this->kids = copyTreeHelper(this->kids, this);
}

CTree* CTree::copyTreeHelper(const CTree* root, CTree *oldroot) {
	if (root == NULL) {
		return NULL;
	}
	else { //copy root node
		CTree* newNode = new CTree(root->data);
		newNode->prev = oldroot;
		newNode->kids = copyTreeHelper(root->kids, newNode);
		newNode->sibs = copyTreeHelper(root->sibs, newNode);
		return newNode;
	}	
}

CTree::~CTree() { //destructor
	if (kids != NULL)
		delete kids;
	if (sibs != NULL)
		delete sibs;
}

//removes a child node (and its children) of a node
bool CTree::removeChild(char ch) {
	//look for the node we want to remove
	//return false if not found
	bool directChild = true;
	CTree* exNode = this->kids;
	
	while (exNode != NULL && exNode->data != ch) {
		directChild = false;
		exNode = exNode->sibs;
	}
	if (exNode == NULL || exNode->data != ch)
		return false;
	
	//if the child node that we are deleting is a "direct child"
	if (directChild) {
		//if has sibling set it prev->kids to sibling and set sib->prev to child's previous
		if (exNode->sibs != NULL) {
			exNode->prev->kids = exNode->sibs;
			exNode->sibs->prev = exNode->prev;
			exNode->sibs = NULL;
			exNode->~CTree(); //when we call this function it is deleting its siblings too, so set its sibs to null above
		}
		//if no sibling set it prev->kids to null and call destructor on that node
		else {				
			exNode->prev->kids = NULL;
			exNode->~CTree();
		}
	}
	//if the child node that we are deleting is not a "direct child"
	else {
		//if has sibling
		if (exNode->sibs != NULL) {
			//set it prev->sibs to child's sibs
			exNode->prev->sibs = exNode->sibs;
			//set it sibs->prev to child's prev
			exNode->sibs->prev = exNode->prev;
			//call destructor
			exNode->sibs = NULL;
			exNode->~CTree(); //when we call this function it is deleting its siblings too, so set its sibs to null above
		}
		//if no sibling
		else {
			//set it prev->sibs to NULL
			exNode->prev->sibs = NULL;
			//call destructor
			exNode->~CTree();
		}
	}
	return true;
}

std::string CTree::toString() {
	std::ostringstream oss; //keep adding characters onto this
	oss << data << "\n"; //add current character onto string
	if (kids != NULL)
		oss << kids->toString();
	if (sibs != NULL)
		oss << sibs->toString();
	return oss.str();
}

bool CTree::addChild(char ch) {
	//check if current node child pointer is empty
	if (this->kids == NULL) { //it is so add new char under it
		this->kids = new CTree(ch);
		this->kids->prev = this;
		return true;
	}
	//check if new character is already child of current node
	else if (kids->data == ch) {
		return false;
	}
	//make ch the child of this node and "slide siblings to the right"
	else if (ch < this->kids->data) {
		CTree* newNode = new CTree(ch);
		newNode->sibs = this->kids;
		newNode->prev = this;
		this->kids = newNode;
		return true;
	}
	else if (kids->prev != NULL)  { //make this character a sibling of the child node since it is not a root node
		return this->kids->addSibling(ch);
	}
	else {
		return false;
	}	
}

bool CTree::addChild(CTree *root) { //root is the thing that will be made the root of this current tree
	//make sure root is a root
	if (root->sibs != NULL || root->prev != NULL)
		return false;
	//checks if current node child pointer is empty
	else if (this->kids == NULL) { //it is so add root under it
		this->kids = root;
		root->prev = this;
		return true;
	}
	else if (kids->data == root->data) {
		return false;
	}
	else if (root->data < this->kids->data) {
		root->sibs = this->kids;
		root->prev = this;
		this->kids = root;
		return true;
	}
	else if (prev != NULL) {
		return this->addSibling(root);
	}
	else {
		return false;
	}
}

bool CTree::addSibling(char ch) {
	CTree *newNode = new CTree(ch);
	return this->addSibling(newNode);
}

bool CTree::addSibling(CTree *root) {
	//make sure root is a root
	if (root->sibs != NULL || root->prev != NULL) {
		delete root;
		return false;
	}
	//make sure we are not adding a sibling to a root
	else if (this->sibs == NULL && this->prev == NULL) {
		delete root;
		return false;
	}
	else if (root->data == this->data) {
		delete root;
		return false;
	}
	else if (root->data < this->data) { //insert node
		root->sibs = this; //give inserted node old node's siblings
		root->prev = this->prev;
		this->prev = root;
		root->prev->sibs = root;
		return true;
	}
	else if (this->sibs == NULL) { //make root a sibling
		this->sibs = root;
		root->prev = this;
		return true;
	}
	else //keep moving through siblings
		return this->sibs->addSibling(root);
}
