/*
Michael Ashmead, 600.120, 4/15/15, Homework 6, 484-682-9355, mashmea1, ashmeadmichael@gmail.com
*/

#ifndef _FILEDIR_H
#define _FILEDIR_H

#include <cstdbool>
#include <string>
#include <sstream> //needed for ostringstream
#include <iostream>
#include <algorithm>

using namespace std;

class FileDir {
	//class members private by default
	string name; //file or directory's name
	long size; //in kilobytes
	bool file; //is the instance of this object a file or a directory

	public:
		FileDir(string, long = 4, bool = false); //main constructor
		FileDir(const  FileDir&);  //copy constructor

		//accessors do not modify object
		//const applies to object calling function
		long getSize() const {
			return size;
		};  
		string getName() const {
			return name;
		};
		bool isFile() const {
			return !file;
		};
		string toString() const { //returns a string version of the object
			ostringstream composite;
			if (file) //if object is directory
				composite << name << "/" << " " << "[" << size << "kb" << "]";
			else
				composite << name << " " << "[" << size << "kb" << "]";
			return composite.str();
		};

		//modifiers (mutators)
		string rename(string); //returns previous name of object
		long resize(long); //returns new size of object

		//overloading operators
		bool operator<(const FileDir& f);
		bool operator==(const FileDir& f);
		friend ostream& operator<<(ostream& output, const FileDir& f);
} ; 

#endif
